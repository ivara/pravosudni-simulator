import tkinter as tk
import random

#functions
def settingsFunction():
    print("Opening settings")

    #create new window
    settingsWindow = tk.Toplevel()
    settingsWindow.title("Postavke")
    settingsWindow.iconphoto(True, icon)
    settingsWindow.geometry("700x780")
    settingsWindow.config(background="#EEEEEE")


    # create main frame
    main_frame = tk.Frame(settingsWindow)
    main_frame.config(background="red")
    main_frame.pack(fill=tk.BOTH, expand=1)

    # create canvas
    my_canvas = tk.Canvas(main_frame)
    my_canvas.config(background="green")
    my_canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    # add scrollbar to canvas
    my_scrollbar = tk.Scrollbar(main_frame, orient=tk.VERTICAL, command=my_canvas.yview)
    my_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    # config canvas to have scrollbar
    my_canvas.configure(yscrollcommand=my_scrollbar.set)
    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion=my_canvas.bbox("all")))
    
    # create frame inside of canvas
    second_frame = tk.Frame(my_canvas)
    second_frame.config(background="blue")

    # add new frame to window in the canvas
    my_canvas.create_window((50,50), window=second_frame)

    #labels
    settingsLabel = tk.Label(second_frame, text="Izuzmi pitanja", font=('Arial', 40, 'bold'),
                fg="#103C55", bg="#EEEEEE")
    categoryLabel = tk.Label(second_frame, text="Kategorija", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
    removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")

    #dropdown menu for category
    def categoryChange(event):

        nonlocal listBoxQuestions
        nonlocal buttonRemove
        nonlocal removedLabel
        nonlocal listBoxRemoved
        nonlocal buttonSave
        nonlocal buttonReturnQ

        listBoxQuestions.pack_forget()
        buttonRemove.pack_forget()
        removedLabel.pack_forget()
        listBoxRemoved.pack_forget()
        buttonSave.pack_forget()
        buttonReturnQ.pack_forget()

        if event == "Kazneno pravo":


            # update list of questions
            listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in kp:
                listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + kp[q])
            listBoxQuestions.pack(pady=10)

            #update buttonRemove
            buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)
            buttonRemove.pack(pady=10)

            #update removedLabel
            removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
            removedLabel.pack(pady=10)

            #update list of removed questions
            listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in kp_remove:
                listBoxRemoved.insert(tk.END, kp_remove[q])
            listBoxRemoved.pack(pady=10)
        elif event == "Radno i upravno":


            # update list of questions
            listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in riu:
                listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + riu[q])
            listBoxQuestions.pack(pady=10)

            #update buttonRemove
            buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)
            buttonRemove.pack(pady=10)

            #update removedLabel
            removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
            removedLabel.pack(pady=10)

            #update list of removed questions
            listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in riu_remove:
                listBoxRemoved.insert(tk.END, riu_remove[q])
            listBoxRemoved.pack(pady=10)
        elif event == "Ustav":
            

            # update list of questions
            listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in ustav:
                listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + ustav[q])
            listBoxQuestions.pack(pady=10)

            #update buttonRemove
            buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)
            buttonRemove.pack(pady=10)

            #update removedLabel
            removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
            removedLabel.pack(pady=10)

            #update list of removed questions
            listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in ustav_remove:
                listBoxRemoved.insert(tk.END, ustav_remove[q])
            listBoxRemoved.pack(pady=10)
        elif event == "Građansko pravo":

            # update list of questions
            listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in gp:
                listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + gp[q])
            listBoxQuestions.pack(pady=10)

            #update buttonRemove
            buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)
            buttonRemove.pack(pady=10)

            #update removedLabel
            removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
            removedLabel.pack(pady=10)

            #update list of removed questions
            listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in gp_remove:
                listBoxRemoved.insert(tk.END, gp_remove[q])
            listBoxRemoved.pack(pady=10)
        elif event == "Građansko procesno pravo":

            # update list of questions
            listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in gpp:
                listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + gpp[q])
            listBoxQuestions.pack(pady=10)

            #update buttonRemove
            buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)
            buttonRemove.pack(pady=10)

            #update removedLabel
            removedLabel = tk.Label(second_frame, text="Izbačena pitanja", font=('Arial', 20, 'bold'),
                fg="#103C55", bg="#EEEEEE")
            removedLabel.pack(pady=10)

            #update list of removed questions
            listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
            for q in gpp_remove:
                listBoxRemoved.insert(tk.END, gpp_remove[q])
            listBoxRemoved.pack(pady=10)
        
        buttonReturnQ = tk.Button(second_frame, text="Vrati", command=returnQ,
                                font=('Arial', 15),
                                padx=5, pady=5 )
        buttonReturnQ.pack(pady=10)
        
        buttonSave = tk.Button(second_frame, text="Završi", command=saveChanges,
                            font=('Arial', 15),
                            padx=10, pady=10)
        buttonSave.pack(pady=10)

    options = ["Kazneno pravo", "Radno i upravno",
                "Ustav", "Građansko pravo", "Građansko procesno pravo"]
    category = tk.StringVar()
    category.set(options[0])
    optionCategory = tk.OptionMenu(second_frame, category, *options, command=categoryChange)
    optionCategory.config(fg="#103C55", bg="#EEEEEE")

    #question list - listbox
    listBoxQuestions = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
    for q in kp:
        listBoxQuestions.insert(tk.END, "  "+str(q)+" - " + kp[q])
    

    #remove question from rotation button

    def removeQ():
        #add to list of removed questions
        if category.get() == "Kazneno pravo":
            index = listBoxQuestions.get(tk.ANCHOR).split(" - ")
            index = index[0].lstrip()
            kp_remove[index] = listBoxQuestions.get(tk.ANCHOR)
            pitanjeR = int(index)
            removedQ = listBoxRemoved.get(0,tk.END)
            newIndex=tk.END
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
            listBoxRemoved.insert(newIndex, listBoxQuestions.get(tk.ANCHOR))
        elif category.get() == "Radno i upravno":
            index = listBoxQuestions.get(tk.ANCHOR).split(" - ")
            index = index[0].lstrip()
            riu_remove[index] = listBoxQuestions.get(tk.ANCHOR)
            pitanjeR = int(index)
            removedQ = listBoxRemoved.get(0,tk.END)
            newIndex=tk.END
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
            listBoxRemoved.insert(newIndex, listBoxQuestions.get(tk.ANCHOR))
        elif category.get() == "Ustav":
            index = listBoxQuestions.get(tk.ANCHOR).split(" - ")
            index = index[0].lstrip()
            ustav_remove[index] = listBoxQuestions.get(tk.ANCHOR)
            pitanjeR = int(index)
            removedQ = listBoxRemoved.get(0,tk.END)
            newIndex=tk.END
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
            listBoxRemoved.insert(newIndex, listBoxQuestions.get(tk.ANCHOR))
        elif category.get() == "Građansko pravo":
            index = listBoxQuestions.get(tk.ANCHOR).split(" - ")
            index = index[0].lstrip()
            gp_remove[index] = listBoxQuestions.get(tk.ANCHOR)
            pitanjeR = int(index)
            removedQ = listBoxRemoved.get(0,tk.END)
            newIndex=tk.END
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
            listBoxRemoved.insert(newIndex, listBoxQuestions.get(tk.ANCHOR))
        elif category.get() == "Građansko procesno pravo":
            index = listBoxQuestions.get(tk.ANCHOR).split(" - ")
            index = index[0].lstrip()
            gpp_remove[index] = listBoxQuestions.get(tk.ANCHOR)
            pitanjeR = int(index)
            removedQ = listBoxRemoved.get(0,tk.END)
            newIndex=tk.END
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
            listBoxRemoved.insert(newIndex, listBoxQuestions.get(tk.ANCHOR))
        listBoxQuestions.delete(tk.ANCHOR)



    buttonRemove = tk.Button(second_frame, text="Izbaci", command=removeQ,
                        font=('Arial', 15),
                        padx=5, pady=5)

    
    #removed questions listbox
    listBoxRemoved = tk.Listbox(second_frame,
                        bd = 2,
                        width=65)
    for q in kp_remove:
        listBoxRemoved.insert(tk.END, kp_remove[q])

    def returnQ():
        #remove from list of removed questions, return question to test rotation
        if category.get() == "Kazneno pravo":
            index, question = listBoxRemoved.get(tk.ANCHOR).split(" - ", 1)
            index = index.lstrip()
            question = question.lstrip()
            #remove from removed list
            kp_remove.pop(index)
            #print(kp_remove)
            # find index for returning question
            pitanjeR = int(index)
            removedQ = listBoxQuestions.get(0,tk.END)
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
                else:
                    newIndex=tk.END

            listBoxQuestions.insert(newIndex, listBoxRemoved.get(tk.ANCHOR))
            listBoxRemoved.delete(tk.ANCHOR)
        
        elif category.get() == "Radno i upravno":
            index, question = listBoxRemoved.get(tk.ANCHOR).split(" - ", 1)
            index = index.lstrip()
            question = question.lstrip()
            #remove from removed list
            riu_remove.pop(index)
            # find index for returning question
            pitanjeR = int(index)
            removedQ = listBoxQuestions.get(0,tk.END)
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
                else:
                    newIndex=tk.END

            listBoxQuestions.insert(newIndex, listBoxRemoved.get(tk.ANCHOR))
            listBoxRemoved.delete(tk.ANCHOR)
        elif category.get() == "Ustav":
            index, question = listBoxRemoved.get(tk.ANCHOR).split(" - ", 1)
            index = index.lstrip()
            question = question.lstrip()
            #remove from removed list
            ustav_remove.pop(index)
            # find index for returning question
            pitanjeR = int(index)
            removedQ = listBoxQuestions.get(0,tk.END)
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
                else:
                    newIndex=tk.END

            listBoxQuestions.insert(newIndex, listBoxRemoved.get(tk.ANCHOR))
            listBoxRemoved.delete(tk.ANCHOR)
        elif category.get() == "Građansko pravo":
            index, question = listBoxRemoved.get(tk.ANCHOR).split(" - ", 1)
            index = index.lstrip()
            question = question.lstrip()
            #remove from removed list
            gp_remove.pop(index)
            # find index for returning question
            pitanjeR = int(index)
            removedQ = listBoxQuestions.get(0,tk.END)
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
                else:
                    newIndex=tk.END

            listBoxQuestions.insert(newIndex, listBoxRemoved.get(tk.ANCHOR))
            listBoxRemoved.delete(tk.ANCHOR)
        elif category.get() == "Građansko procesno pravo":
            index, question = listBoxRemoved.get(tk.ANCHOR).split(" - ", 1)
            index = index.lstrip()
            question = question.lstrip()
            #remove from removed list
            gpp_remove.pop(index)
            # find index for returning question
            pitanjeR = int(index)
            removedQ = listBoxQuestions.get(0,tk.END)
            for p in removedQ:
                qIndex = p.split(" - ")[0]
                qIndex = int(qIndex.lstrip())
                if(pitanjeR < qIndex):
                    newIndex = removedQ.index(p)
                    break
                else:
                    newIndex=tk.END

            listBoxQuestions.insert(newIndex, listBoxRemoved.get(tk.ANCHOR))
            listBoxRemoved.delete(tk.ANCHOR)
        #listBoxQuestions.delete(tk.ANCHOR)
    
    buttonReturnQ = tk.Button(second_frame, text="Vrati", command=returnQ,
                                font=('Arial', 15),
                                padx=5, pady=5 )


    def saveChanges():
        for k,v in kp_remove.items():
            kp_removed.append(int(k))
        for k,v in gp_remove.items():
            gp_removed.append(int(k))
        for k,v in gpp_remove.items():
            gpp_removed.append(int(k))
        for k,v in riu_remove.items():
            riu_removed.append(int(k))
        for k,v in ustav_remove.items():
            ustav_removed.append(int(k))
        
        #remove duplicates
        

        #exit screen
        settingsWindow.destroy()
        
    # save changes button 
    buttonSave = tk.Button(second_frame, text="Spremi", command=saveChanges,
                            font=('Arial', 15),
                            padx=10, pady=10)                    

    #display parts
    settingsLabel.pack(padx=10, pady=10)
    categoryLabel.pack(pady=5)
    optionCategory.pack(pady=10)
    listBoxQuestions.pack(pady=10)
    buttonRemove.pack(pady=10)
    removedLabel.pack(pady=10)
    listBoxRemoved.pack(pady=10)
    buttonReturnQ.pack(pady=10)
    buttonSave.pack(pady=10)


def startTest():
    global kp_removed
    global riu_removed
    global ustav_removed
    global gp_removed
    global gpp_removed

    kp_removed = list(dict.fromkeys(kp_removed))
    riu_removed = list(dict.fromkeys(riu_removed))
    ustav_removed = list(dict.fromkeys(ustav_removed))
    gp_removed = list(dict.fromkeys(gp_removed))
    gpp_removed = list(dict.fromkeys(gpp_removed))
    

    print()
    print("==== Starting test ====")
    if(len(kp_removed) or len(riu_removed) or len(ustav_removed) or len(gp_removed) or len(gpp_removed)):
        
        print("Removed questions: ")
        print("\n Kazneno pravo:", end =" ")
        for k in kp_removed:
            print(k, end=" ")
        print("\n Radno i upravno pravo:", end=" ")
        for k in riu_removed:
            print(k, end=" ")
        print("\n Ustav:", end=" ")
        for k in ustav_removed:
            print(k, end=" ")
        print("\n Građansko pravo:", end=" ")
        for k in gp_removed:
            print(k, end=" ")
        print("\n Građansko procesno pravo:", end=" ")
        for k in gpp_removed:
            print(k, end=" ")
        print()


    # create new window
    testWindow = tk.Toplevel()
    testWindow.title("Sretno")
    testWindow.iconphoto(True, icon)
    testWindow.geometry("700x500")
    testWindow.config(background="#103C55")

    # init vars
    questions = {0: [], 1: [], 2: [], 3: [], 4: []}
    labels = ['Kazneno pravo', 'Radno i upravno pravo', 'Ustav', 'Građansko pravo', 'Građansko procesno pravo']

    # program 
    # kp choice
    if(len(kp_removed)):
        avQ = list(range(1,32))
        avQ = list(set(avQ) - set(kp_removed))
        random.shuffle(avQ)
        qs = avQ[0:3]
        avQ = list(range(32,72))
        avQ = list(set(avQ) - set(kp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:3])
        qs.sort(reverse=True)
        for q in qs:
            questions[0].append(str(q) + " // " + kp[q])        
    else:
        qs = random.sample(range(1, 31), 3)
        qs.extend(random.sample(range(32, 71), 3))
        qs.sort(reverse=True)
        for q in qs:
            questions[0].append(str(q) + " // " + kp[q])

    # riu choice
    if(len(riu_removed)):
        # 1. grupa
        avQ = list(range(1,20))
        avQ = list(set(avQ) - set(riu_removed))
        random.shuffle(avQ)
        qs = avQ[0:1]
        # 2. grupa
        avQ = list(range(20,32))
        avQ = list(set(avQ) - set(riu_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:2])
        # 3. grupa
        avQ = list(range(32,49))
        avQ = list(set(avQ) - set(riu_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:2])
        # 4. grupa
        avQ = list(range(49,57))
        avQ = list(set(avQ) - set(riu_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])
        qs.sort(reverse=True)
        for q in qs:
            questions[1].append(str(q) + " // " + riu[q])        
    else:
        qs = random.sample(range(1, 19), 1)
        qs.extend(random.sample(range(20, 31), 2))
        qs.extend(random.sample(range(32, 48), 2))
        qs.extend(random.sample(range(49, 56), 1))
        qs.sort(reverse=True)
        for q in qs:
            questions[1].append(str(q) + " // " + riu[q])

    # ustav choice
    if(len(ustav_removed)):
        # 1. grupa
        avQ = list(range(1,34))
        avQ = list(set(avQ) - set(ustav_removed))
        random.shuffle(avQ)
        qs = avQ[0:2]
        # 2. grupa
        avQ = list(range(34,65))
        avQ = list(set(avQ) - set(ustav_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:2])
        # 3. grupa
        avQ = list(range(65,95))
        avQ = list(set(avQ) - set(ustav_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:2])
        
        qs.sort(reverse=True)
        for q in qs:
            questions[2].append(str(q) + " // " + ustav[q])
    
    else:
        qs = random.sample(range(1, 33), 2)
        qs.extend(random.sample(range(34, 64), 2))
        qs.extend(random.sample(range(65, 94), 2))
        qs.sort(reverse=True)
        for q in qs:
            questions[2].append(str(q) + " // " + ustav[q])

    #gp choice
    if(len(gp_removed)):
        # 1. grupa
        avQ = list(range(1,28))
        avQ = list(set(avQ) - set(gp_removed))
        random.shuffle(avQ)
        qs = avQ[0:2]
        # 2. grupa
        avQ = list(range(28,42))
        avQ = list(set(avQ) - set(gp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])
        # 3. grupa
        avQ = list(range(42,51))
        avQ = list(set(avQ) - set(gp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])
        # 4. grupa
        avQ = list(range(51,62))
        avQ = list(set(avQ) - set(gp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])
        # 5. grupa
        avQ = list(range(62,75))
        avQ = list(set(avQ) - set(gp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])

        qs.sort(reverse=True)
        for q in qs:
            questions[3].append(str(q) + " // " + gp[q])

    else:
        qs = random.sample(range(1, 27), 2)
        qs.extend(random.sample(range(28, 41), 1))
        qs.extend(random.sample(range(42, 50), 1))
        qs.extend(random.sample(range(51, 61), 1))
        qs.extend(random.sample(range(62, 74), 1))
        qs.sort(reverse=True)
        for q in qs:
            questions[3].append(str(q) + " // " + gp[q])

    #gpp choice
    if(len(gpp_removed)):
        # 1. grupa
        avQ = list(range(1,45))
        avQ = list(set(avQ) - set(gpp_removed))
        random.shuffle(avQ)
        qs = avQ[0:3]
        # 2. grupa
        avQ = list(range(45,72))
        avQ = list(set(avQ) - set(gpp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:1])
        # 3. grupa
        avQ = list(range(72,87))
        avQ = list(set(avQ) - set(gpp_removed))
        random.shuffle(avQ)
        qs.extend(avQ[0:2])
        
        qs.sort(reverse=True)
        for q in qs:
            questions[4].append(str(q) + " // " + gpp[q])

    else:
        qs = random.sample(range(1, 44), 3)
        qs.extend(random.sample(range(45, 71), 1))
        qs.extend(random.sample(range(72, 87), 2))
        qs.sort(reverse=True)
        for q in qs:
            questions[4].append(str(q) + " // " + gpp[q])

    #display
    sectionLabel = tk.Label(testWindow,
                text=labels[0],
                font=('Arial', 40, 'bold'),
                fg="#F1F3F4", bg="#103C55"
                )
    sectionLabel.pack(padx=10, pady=20)

    #scroll
    scrollH = tk.Scrollbar(testWindow, orient="vertical")
    #scrollH.pack(side=tk.RIGHT, fill="y")

    #text-widget
    questionsText = tk.Text(testWindow, 
                            font=('Arial', 20),
                            yscrollcommand=scrollH.set,
                            fg="#F1F3F4", bg="#103C55",
                            width=20, height=6,
                            wrap=tk.WORD
                            )
    questionsText.tag_configure("centerText", justify='center')
    questionsText.tag_configure("whole", spacing1=20)


    #add questions to text-widget
    for i in range(6):
        questionsText.insert("1.0", 
                            questions[0][i])
        
        questionsText.tag_add("centerText", "1.0", "end")
        questionsText.tag_add("whole", "2.0", "end-1c")


    
    #attach scroll to text-widget
    scrollH.config(command=questionsText.yview)

    questionsText.pack(fill=tk.BOTH, expand=1, padx=10, pady=10)
    
    def forward(section): 
             
        nonlocal button_next
        nonlocal button_prev
        nonlocal sectionLabel
        nonlocal questionsText
        nonlocal scrollH

        # remove prev page
        sectionLabel.pack_forget()
        button_next.pack_forget()
        button_prev.pack_forget()
        questionsText.destroy()
        scrollH.pack_forget()

        # create new page
        # title
        sectionLabel = tk.Label(testWindow,
                text=labels[section],
                font=('Arial', 40, 'bold'),
                fg="#F1F3F4", bg="#103C55"
                )
        sectionLabel.pack(padx=10, pady=20)
        #questions
        questionsText = tk.Text(testWindow, 
                            font=('Arial', 20),
                            yscrollcommand=scrollH.set,
                            fg="#F1F3F4", bg="#103C55",
                            width=20, height=6,
                            wrap=tk.WORD
                            )
        questionsText.tag_configure("centerText", justify='center')
        questionsText.tag_configure("whole", spacing1=20)
        for i in range(6):
            try:
                questionsText.insert("1.0", 
                                    questions[section][i])
                questionsText.tag_add("centerText", "1.0", "end")
                questionsText.tag_add("whole", "2.0", "end-1c")
            except:
                questionsText.insert("1.0", 
                                    " ** Nedovoljno dopuštenih pitanja ** \n")
                questionsText.tag_add("centerText", "1.0", "end")
                questionsText.tag_add("whole", "2.0", "end-1c")
        
        scrollH.config(command=questionsText.yview)
        questionsText.pack(fill=tk.BOTH, expand=1, padx=10, pady=10)
        #buttons
        if section == 0:
            button_prev = tk.Button(testWindow, text="<<", state = tk.DISABLED)
        else:
            button_prev = tk.Button(testWindow, text="<<", command=lambda: back(section-1))
        if section == 4:
            button_next = tk.Button(testWindow, text=">>", state=tk.DISABLED)
        else:
            button_next = tk.Button(testWindow, text=">>", command=lambda: forward(section+1))
            
        button_prev.pack(side=tk.LEFT,padx=30, pady=20)
        button_next.pack(side=tk.RIGHT,padx=30, pady=20)

    def back(section):
        nonlocal button_next
        nonlocal button_prev
        nonlocal sectionLabel
        nonlocal questionsText
        nonlocal scrollH

        # remove prev page
        sectionLabel.pack_forget()
        button_next.pack_forget()
        button_prev.pack_forget()
        questionsText.destroy()
        scrollH.pack_forget()

        # create new page
        # title
        sectionLabel = tk.Label(testWindow,
                text=labels[section],
                font=('Arial', 40, 'bold'),
                fg="#F1F3F4", bg="#103C55"
                )
        sectionLabel.pack(padx=10, pady=20)
        #questions
        questionsText = tk.Text(testWindow, 
                            font=('Arial', 20),
                            yscrollcommand=scrollH.set,
                            fg="#F1F3F4", bg="#103C55",
                            width=20, height=6,
                            wrap=tk.WORD
                            )
        questionsText.tag_configure("centerText", justify='center')
        questionsText.tag_configure("whole", spacing1=20)
        for i in range(6):
            try:
                questionsText.insert("1.0", 
                                    questions[section][i])
                questionsText.tag_add("centerText", "1.0", "end")
                questionsText.tag_add("whole", "2.0", "end-1c")
            except:
                questionsText.insert("1.0", 
                                    " *** Nedovoljno dopuštenih pitanja *** \n")
                questionsText.tag_add("centerText", "1.0", "end")
                questionsText.tag_add("whole", "2.0", "end-1c")
        
        scrollH.config(command=questionsText.yview)
        questionsText.pack(fill=tk.BOTH, expand=1, padx=10, pady=10)
        #buttons
        if section == 0:
            button_prev = tk.Button(testWindow, text="<<", state = tk.DISABLED)
        else:
            button_prev = tk.Button(testWindow, text="<<", command=lambda: back(section-1))
        if section == 4:
            button_next = tk.Button(testWindow, text=">>", state=tk.DISABLED)
        else:
            button_next = tk.Button(testWindow, text=">>", command=lambda: forward(section+1))
            
        button_prev.pack(side=tk.LEFT,padx=30, pady=20)
        button_next.pack(side=tk.RIGHT,padx=30, pady=20)

    button_prev = tk.Button(testWindow, text="<<", command=back, state=tk.DISABLED)
    button_prev.pack(side=tk.LEFT,padx=30, pady=20)
    button_next = tk.Button(testWindow, text=">>", command=lambda: forward(1))
    button_next.pack(side=tk.RIGHT,padx=30, pady=20)


# setup data
#init vars
gp = {}
kp = {}
riu = {}
gpp = {}
ustav = {}

kp_remove = {}
gp_remove = {}
gpp_remove = {}
riu_remove = {}
ustav_remove = {}

gp_removed = []
kp_removed = []
riu_removed = []
gpp_removed = []
ustav_removed = []


# collect questions into dictionaries from .txt files
#gp
with open('pitanja_gp.txt', 'r') as f:
    qs = f.readlines()
i = 1
for q in qs:
    gp[i] = q
    i += 1
#kp
with open('pitanja_kp.txt', 'r') as f:
    qs = f.readlines()
i = 1
for q in qs:
    kp[i] = q
    i += 1
#riu
with open('pitanja_riu.txt', 'r') as f:
    qs = f.readlines()
i = 1
for q in qs:
    riu[i] = q
    i += 1
#ustav
with open('pitanja_ustav.txt', 'r') as f:
    qs = f.readlines()
i = 1
for q in qs:
    ustav[i] = q
    i += 1
#gpp
with open('pitanja_gpp.txt', 'r') as f:
    qs = f.readlines()
i = 1
for q in qs:
    gpp[i] = q
    i += 1

 
    
# setup main window
root = tk.Tk()
root.geometry("700x450")
root.title("Pravosudni simulator")
# icon
icon = tk.PhotoImage(file="icon.png")
root.iconphoto(True, icon)
# background color
root.config(background="#103C55")

picture = tk.PhotoImage(file='icon.png')

titleLabel = tk.Label(root,
                text="Pravosudni simulator",
                font=('Arial', 40, 'bold'),
                fg="#F1F3F4", bg="#103C55"
                ) #image=picture,
                #compound='bottom'
titleLabel.pack(padx=10, pady=10)
iconLabel = tk.Label(root,
                image=picture,
                bg="#103C55")
iconLabel.pack()

settingsButton = tk.Button(root,
                text="Postavke",
                command=settingsFunction,
                font=('Arial', 15),
                fg="#F1F3F4", bg="#4F4F4F", highlightbackground="black",
                padx=10, pady=10)

settingsButton.pack(side=tk.LEFT,padx=30)

goButton = tk.Button(root,
            text="Kreni",
            command=startTest,
            font=('Arial', 15),
            fg="#F1F3F4", bg="#B5C588", highlightbackground="black",
            padx=10, pady=10)

goButton.pack(side=tk.RIGHT, padx=30)

root.mainloop()   #create window